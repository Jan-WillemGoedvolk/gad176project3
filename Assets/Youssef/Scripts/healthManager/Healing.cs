using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Made By Youssef with help from J.W
/// Heals the orbiter by the given amount when collided with
/// </summary>
public class Healing : HealthManager
{
    #region Variables
    [Header("Healing")]
    [SerializeField] private float healAmount = 5f;
    #endregion

    #region Unity Specific Functions
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log($"{gameObject.name} collided with {collision.gameObject.name}");
        if (collision.gameObject.CompareTag("Heal")) // If we hit the orbiter
        {
            HealPlayer(gameObject.GetComponent<HealthManager>()); // Heal the orbiter
            Debug.Log(($"Healed player for {healAmount}"));
            Destroy(collision.gameObject);
        }
    }
    #endregion

    #region Private Functions
    /// <summary>
    /// Heals the orbiter by the set amount
    /// </summary>
    private void HealPlayer(HealthManager toHeal)
    {
        //calls the function 
        toHeal.ChangeHealth(healAmount);
    } 
    #endregion
}
