using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invincible : HealthManager
{
    private bool isInvincible = false;
    private float timer = 0f;
    private float duration = 10f;

    public void ActivateInvincibility()
    {
        isInvincible = true;

        timer = duration;

    }

    public void CHangeHealth(int changeBy)
    {
        if (!isInvincible)
        {
            Health = changeBy;
        }
    }
   

    // Update is called once per frame
    void Update()
    {
        if (isInvincible)
        {
            timer -= Time.deltaTime;
            if(timer <= 0f)
            {
                isInvincible = false;
            }
        }
    }
}
