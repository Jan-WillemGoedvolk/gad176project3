using JW.Project3.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Made By Youssef with help from J.W
/// Manages health for an object and is used to provide healing through inheritance
/// </summary>
public class HealthManager : MonoBehaviour
{
    #region Variables
    [Header("Health Manager")]
    //set the max health and set the health var
    [SerializeField] protected float maxHealth = 10f;
    [SerializeField] protected float health;
    [SerializeField] private GameEvent playerDeath;

    #region Getters and Setters
    //to control the health value 
    public float Health
    {
        get { return health; }
        set
        {
            //to heal th eplayer
            health += value;

            //clamp the health so it wont go above the max health 
            health = Mathf.Clamp(health, 0, maxHealth);

            // Check for death
            if (health <= 0)
            {
                Debug.Log("Destroyed healthmanager");
                playerDeath.Raise();
            }
        }
    }
    #endregion
    #endregion

    #region Public Functions
    //the orbiter either losses or gains health when this function is called in other scripits
    public void ChangeHealth(float ammount)
    {
        Health = ammount;
    }
    #endregion

    #region Unity Specific Functions
    private void OnEnable()
    {
        health = maxHealth;
    }
    #endregion
}
