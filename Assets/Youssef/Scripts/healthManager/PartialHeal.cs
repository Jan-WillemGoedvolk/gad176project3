using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Made By Youssef with help from J.W
/// Heals the orbiter when it collides with it by a percentage of its current health
/// </summary>
public class PartialHeal : HealthManager
{
    #region Variables
    [Header("Partial Healing")]
    [SerializeField] private float healPercentage = 0.5f; // Percentage of orbiter health to heal for
    #endregion

    #region Unity Specific Functions
    //collide with the orbiter with tag orbiter to call the function partialhealplayer that calls the override function
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log($"{gameObject.name} collided with {collision.gameObject.name}");
        if (collision.gameObject.CompareTag("PartialHeal")) // Check for collision with orbiter
        {
            PartialHealPlayer(gameObject.GetComponent<HealthManager>()); // Heal orbiter
            Debug.Log(("Healed player partially"));
            Destroy(collision.gameObject);
        }
    }
    #endregion
    
    #region Private Functions
    private void PartialHealPlayer(HealthManager toHeal)
    {
        // Calculate how much to heal
        float healAmount = Health * healPercentage;
        healAmount = Mathf.Round(healAmount);

        // Apply healing
        toHeal.ChangeHealth(healAmount);
    } 
    #endregion
}
