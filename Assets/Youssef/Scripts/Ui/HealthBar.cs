using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI healthText;
    [SerializeField] private HealthManager healthManager; // Reference to the HealthManager script

    private void OnEnable()
    {
        UpdateHealthText();
    }

    void Update()
    {
        UpdateHealthText();
    }

    void UpdateHealthText()
    {
        healthText.text = "Health: " + healthManager.Health.ToString(); // Update the Text Mesh Pro with the current health from HealthManager
    }
}