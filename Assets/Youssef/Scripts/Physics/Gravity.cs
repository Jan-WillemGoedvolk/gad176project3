using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Made By Youssef 
/// Calculates and can apply gravity like pulling towards a GameObject
/// </summary>
public class Gravity : MonoBehaviour
{
    #region Variables
    //Add orbiter object in heiracy
    [SerializeField] protected Transform orbiter;
    [SerializeField] protected Rigidbody2D orbiterRb;
    [SerializeField] protected Transform planet;

    //Varriables for orbiting/Gravity simulaton
    [SerializeField] private float range;
    [SerializeField] private float intensity = 1f;
    [SerializeField] private float distanceFromPlayer;
    [SerializeField] Vector2 gravForce;
    #endregion

    #region Public Functions
    /// <summary>
    /// Calculate the Vector2D towards the planet for gravity
    /// </summary>
    /// <param name="planet">Transform | Transform of the object to orbit</param>
    /// <param name="orbiter">Transform | Transform of the thing that is orbiting the planet</param>
    /// <returns>Vector2 | Vector for gracity pull towards the planet based on distance between them</returns>
    public Vector2 CalculateGravity(Transform planet, Transform orbiter)
    {
        float distanceBetweenObjects = Vector2.Distance(planet.position, orbiter.position);
        Vector2 gravity = (planet.position - orbiter.position).normalized / distanceBetweenObjects * intensity; // Based on Youssef's equation
        return gravity; // Give back the calculated vector
    }
    #endregion

    #region Unity Specific Functions
    private void OnEnable()
    {
        Debug.Log("Gravit OnEnable");
        orbiter = gameObject.GetComponent<Transform>();
        orbiterRb = gameObject.GetComponent<Rigidbody2D>();
        planet = GameObject.FindGameObjectWithTag("Planet").GetComponent<Transform>();
    }

    //Orbiting/Gravity simulation
    protected virtual void Update()
    {
        //Distance between orbitter and planet
        distanceFromPlayer = Vector2.Distance(orbiter.position, planet.position);
        if (distanceFromPlayer < range)
        {
            gravForce = CalculateGravity(planet, orbiter);
            orbiterRb.AddForce(gravForce, ForceMode2D.Force);
        }
    } 
    #endregion
}
