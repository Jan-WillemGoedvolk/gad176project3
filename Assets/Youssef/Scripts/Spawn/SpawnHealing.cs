using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Made By Youssef 
/// Script to spawn healling powerups within a given area
/// </summary>
public class SpawnHealing : MonoBehaviour
{
    #region Variables
    [Header(("Prefabs"))]
    //var to hold gameobjects
    [SerializeField] private GameObject partialHeal;
    [SerializeField] private GameObject healing;

    [Header("Spawn Area")]
    //var to define gizmos area in scene
    [SerializeField] private Vector2 center;
    [SerializeField] private Vector2 size;

    [Header(("Timer"))]
    //control spawn time var
    [SerializeField] private float spawnTime;
    #endregion

    #region Public Functions
    /// <summary>
    /// Spawns the power ups in a random location within the spawn area
    /// </summary>
    public void SpawnPowerUp()
    {
        //genartes random position with the specifed region
        Vector2 pos = center + new Vector2(Random.Range(-size.x / 2, size.x / 2), Random.Range(-size.y / 2, size.y / 2));

        //spawn the gamobjects within the specifed are 
        Instantiate(partialHeal, pos, Quaternion.identity);
        Instantiate(healing, pos, Quaternion.identity);
    }
    #endregion

    #region Unity Specific Functions
    private void Start()
    {
        //  spawning objects after a short delay
        InvokeRepeating("SpawnPowerUp", 1f, spawnTime);
    }
    #endregion

    #region Debugging
    //gizmos to help map out he region
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(center, size);
    } 
    #endregion

}
