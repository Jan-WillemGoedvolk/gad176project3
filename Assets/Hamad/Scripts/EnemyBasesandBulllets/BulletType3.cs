using System.Collections.Generic;
using UnityEngine;


namespace Hamad.Project3.Bullets
{

    /// <summary>
    /// Author: Hamad
    /// Type 3 bullet behavior: Chases the player down without slowing down
    /// </summary>


    public class BulletType3 : NewBaseBullet
    {

        [SerializeField] private float lifetime; //the amount of time the bullet stays in the scene
        [SerializeField] private float speed; //the speed of the bullet
        [SerializeField] private GameObject player; //for the bullet to follow the player

        void Start()
        {
            player = GameObject.FindWithTag("Player"); //searches for the player in the hierarchy of the scene
        }


        void Update()
        {

            if (player == null) //failsafe statement
            {
                Debug.Log("Warning: BulletType1 doesn't have a Player Reference");
                return;
            }

            Move();

            lifetime -= Time.deltaTime;
            if (lifetime <= 0)
                DestroyBullet(); //calls the function in the inherited script to destroy the bullet

        }

        public override void Move()
        {
            Vector2 direction = player.transform.position - transform.position; //get the direction that the bullet needs to head to the player

            transform.Translate(direction.normalized * speed * Time.deltaTime); //moves towards the player with the required speed
        }

        

    }

}