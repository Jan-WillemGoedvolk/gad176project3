using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Hamad.Project3.Enemy {


    /// <summary>
    /// Enemy Base Scriptable Object: determines the health, bulletType to be shot, cooldownTimer till the next bullet is shot, and Color of the sprite
    /// </summary>

[CreateAssetMenu(fileName = "New Enemy Base", menuName = "Enemy Base")]

    public class EnemyBase : ScriptableObject
    {

        public int health; //health of the enemybase
        public GameObject bulletType; //bullettype to be shot

        public float cooldownTimer; //timer till the next bullet is shot after the previous one
        public float shotCooldownTimer; //timer till the enemy starts shooting again

        public Color color;  //the color of the enemybase type


    }

}