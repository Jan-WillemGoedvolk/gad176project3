using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Hamad.Project3.Bullets
{

    /// <summary>
    /// Author: Hamad
    /// Type 2 Bullet Behavior: Approaches the player and slows down if it is really close to the player
    /// </summary>


    public class BulletType2 : NewBaseBullet
    {

        [SerializeField] private Rigidbody2D _rb2D;
        [SerializeField] private float lifetime;
        [SerializeField] private GameObject player;


        void Start()
        {
            player = GameObject.FindWithTag("Player"); //finds the player in the hierarchy of the scene
        }


        void Update()
        {

            if (_rb2D == null && player == null) //failsafe statement
            {
                Debug.Log("Warning: BulletType1 doesn't have a Rigidbody and/or Player Reference");
                return;
            }

            Move();

            lifetime -= Time.deltaTime;
            if (lifetime <= 0)
                DestroyBullet(); //calls the function in the inherited script to destroy the bullet

        }

        public override void Move()
        {
            Vector2 direction = player.transform.position - transform.position; //get the direction that the bullet needs to head to the player

            _rb2D.velocity = new Vector2(direction.x, direction.y); //moves faster and slower depending on the value of direction

        }

        

    }

}