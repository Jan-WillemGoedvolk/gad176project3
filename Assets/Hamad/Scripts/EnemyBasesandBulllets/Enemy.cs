using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hamad.Project3.Enemy
{

    /// <summary>
    /// Author: Hamad and JW
    /// Enemy Base Behaviour: Has a Raycast on top of it that detects the Player
    /// if the player is detected, it shoots a bullet from the scriptable object attached to it
    /// </summary>


    public class Enemy : MonoBehaviour
    {

        [SerializeField] private EnemyBase baseType; //gets the basetype
        [SerializeField] private bool canShoot; //bool to check if the enemyBase can shoot bullets 
        [SerializeField] private float timer; //for use with the cooldowntimer from the basetype
        [SerializeField] private SpriteRenderer sprite; //for use to change the color of the enemybase sprite
        private bool shot; //to check if the enemy is shot by the player
        private float timer2; //for use with the shotCooldown
        

        void Start()
        {
            timer = baseType.cooldownTimer; //sets the timer to the cooldown designated from the Scriptable Object
            sprite.GetComponent<SpriteRenderer>(); //gets the spriterenderer component from the enemybase
            sprite.color = baseType.color;  //sets the color of the sprite to the color designated from the Scriptable Object
            shot = false; //set as false so that enemybases attack from the beginning
            timer2 = baseType.shotCooldownTimer; //assigning the same value so that the enemybases can attack from the beginning
        }

        void Update()
        {

            if (sprite == null) //failsafe statement
            {
                Debug.Log("Warning: Enemybase doesn't have Sprite Renderer component");
                return;
            }

            timer -= Time.deltaTime; //for cooldown after each bullet is shot
            timer2 += Time.deltaTime; //for cooldown when enemybases are shot by the player
            RaycastHit2D test = Physics2D.Raycast(transform.position, transform.up, 1000f); //for use to get the tag of the object hit with the raycast
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.up), Color.red); //for debugging the raycast
            if (test.collider != null)
            {

                if (test.collider.CompareTag("Player") && timer <= 0 && !shot) //if the player was hit by the raycast while the cooldown is off
                {
                    Debug.Log("Player detected");
                    Instantiate(baseType.bulletType, transform.position, Quaternion.identity); //shoots the bullettype
                    timer = baseType.cooldownTimer; //resets the cooldowntimer
                }
                else
                {
                    Debug.Log("Not player or cooldown is on");
                }

            }
            else Debug.Log("Nothing detected");


            if (timer2 > baseType.shotCooldownTimer)
            {
                shot = false; //allows the enemybase to shoot again
            }

            



        }


		private void OnTriggerEnter2D(Collider2D collision)
		{
            if (collision.CompareTag("PlayerBullet")) {
                shot = true;
                timer2 = 0; //stops the enemy base from shooting for a time determined by shotCooldown
                    }

		}




	}

}