using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Hamad.Project3.Bullets
{

	/// <summary>
	/// Authors: Hamad and JW
	/// Base Bullet script for other bullets to inherit from
	/// Has two functions: One for Destroying the bullet and one for checking if the player collided with the bullet
	/// </summary>

	public class NewBaseBullet : MonoBehaviour
	{
		protected float damage = 1f;

		public virtual void Move()
		{

			Debug.Log("moving"); //in case the Move() function doesn't get ovverrided by another bullet

		}

		private void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.CompareTag("Player"))
			{
				var playerHealtrh = collision.gameObject.GetComponent<HealthManager>(); //gets the healthmanager of the player
				playerHealtrh.ChangeHealth(-damage); //deducts health from the player
				Destroy(gameObject); //destroys the bullet
			}
		}

		public virtual void DestroyBullet()
		{
			Destroy(gameObject); //destroys the bullet
		}
	}

}