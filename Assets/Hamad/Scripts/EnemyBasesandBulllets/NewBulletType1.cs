using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hamad.Project3.Bullets
{

    /// <summary>
    /// Author: Hamad
    /// Type 1 Bullet Behaviour: Gets shot straight at the direction that the player was at initially
    /// </summary>

    public class NewBulletType1 : NewBaseBullet
    {
        [SerializeField] private Rigidbody2D _rb2D; //for use to add velocity to the bullet
        [SerializeField] private float lifetime;  //the amount of time the bullet is present in the scene
        [SerializeField] private float speed;  //the speed of the bullet
        [SerializeField] private GameObject player; //for use to get the Transform of the player in Start()
        private Vector2 direction; //getting the direction from the bullet to the player

        private void Awake()
        {
            player = GameObject.FindWithTag("Player");  //gets the player in the hierarchy of the scene
        }

        void Start()
        {
            _rb2D = GetComponent<Rigidbody2D>();
            direction = player.transform.position - transform.position; //gets the direction from the bullet to the player in the initial position
        }


        void Update()
        {

            if (_rb2D == null && player == null) //failsafe statement
            {
                Debug.Log("Warning: BulletType1 doesn't have a Rigidbody and/or Reference");
                return;
            }

            Move();

            lifetime -= Time.deltaTime;
            if (lifetime <= 0)
                DestroyBullet(); //calls the function in the inherited script to destroy the bullet


        }

        public override void Move()
        {

            _rb2D.velocity = new Vector2(direction.x * speed, direction.y * speed); //moves the bullet in the direction that the player was in initially

        }

        
    }

}