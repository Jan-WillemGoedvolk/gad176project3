using JW.Project3.Movement.Player;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace JW.Project3.Weapons
{
    /// <summary>
    /// Author: JW
    /// The Monobehaviour script for all bullet GameObjects to be able to detect collision and move downard
    /// </summary>
    public class AmmoMono : Gravity
    {
        #region Variables
        [Header("Movement")]
        [SerializeField] private float speed = 0.1f;
        [SerializeField] private Rigidbody2D rb;
        [Header("Bullet Item")]
        [SerializeField] private AmmoScriptable ammo;
        #endregion

        #region Public Functions
        /// <summary>
        /// Sets up the necesarry components
        /// </summary>
        /// <param name="equipedAmmo">AmmoScriptable | The currently equiped ammo item</param>
        public void Setup(AmmoScriptable equipedAmmo)
        {
            Debug.Log("AmmoMono Setup");
            rb = GetComponent<Rigidbody2D>();
            ammo = equipedAmmo;
            speed = ammo.Speed;
            planet = GameObject.FindGameObjectWithTag("Planet").GetComponent<Transform>();
            orbiter = transform;
            orbiterRb = rb;
        }
        #endregion

        #region Unity Specific Functions
        private void OnEnable()
        {
            Debug.Log("AmmoMono OnEnable");
            // Movement
            planet = GameObject.FindGameObjectWithTag("Planet").GetComponent<Transform>();
            orbiter = transform;
        }

        protected override void Update()
        {
            rb.AddForce(CalculateGravity(planet, transform) * speed); // Calculate and apply gravity force
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            // Do damage stuff
            if (collision.gameObject.CompareTag("Planet"))
            {
                Destroy(gameObject);
            }
        }
        #endregion

        #region Debugging
        private void OnDrawGizmos()
        {
            Debug.DrawRay(transform.position, transform.forward);
        }
        #endregion
    }
}
