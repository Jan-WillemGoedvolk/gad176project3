using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Project3.Weapons
{
    /// <summary>
    /// Author: JW
    /// Holds the logic and functionality for the weapons and ammo and the currently equiped ones
    /// </summary>
    public class Weapons : MonoBehaviour
    {
        #region Variables
        [Header("Inputs")]
        [SerializeField] private List<KeyCode> fireKeys = new List<KeyCode>(); // All the keys to fire the weapon
        [Header("Current Weapon")]
        [SerializeField] private int currentIndexWeapon = 0;
        [SerializeField] private int currentIndexAmmo = 0;
        [SerializeField] private WeaponScriptable currentWeapon;
        [SerializeField] private AmmoScriptable currentAmmo;
        [Header("Weapon List")]
        [SerializeField] private List<WeaponScriptable> weapons = new List<WeaponScriptable>();
        [SerializeField] private List<AmmoScriptable> ammos = new List<AmmoScriptable>();
        #endregion

        #region Public Functions
        /// <summary>
        /// Spawns and sets up a bullet
        /// </summary>
        public void Shoot()
        {
            AmmoMono firedAmmo = Instantiate(currentAmmo.Prefab, transform.position + -transform.up*2, Quaternion.identity).GetComponent<AmmoMono>();
            firedAmmo.Setup(currentAmmo);
        }
        #endregion

        #region Unity Specific Functions
        private void OnEnable()
        {
            if (weapons.Count == 0) Debug.LogError($"ERROR |{name}| No weapon items in the list");

            // Reset currently equiped weapon
            currentIndexWeapon = 0;
            currentIndexAmmo = 0;
            currentWeapon = weapons[currentIndexWeapon];
            currentAmmo = ammos[currentIndexAmmo];
            currentWeapon.LoadDefualts(); // Reset everything to its defualt values
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            // Fire the weapon
            foreach (var key in fireKeys)
            {
                if (Input.GetKey(key))
                {
                    if (currentWeapon.UpdateCooldown(Time.deltaTime)) // Update and check fire rate cooldown
                    {
                        Debug.Log("Shoot bullet");
                        Shoot();
                    }
                }
            }
        }
        #endregion
    }
}
