using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Project3.Weapons
{
    /// <summary>
    /// Author: JW
    /// ScriptableObjecct template for Player bullets
    /// </summary>
    [CreateAssetMenu(fileName = "Ammo", menuName = "ScriptableObjects/Player/Ammo", order = 2)]
    public class AmmoScriptable : ScriptableObject
    {
        #region Variables
        [Header("Model")]
        [SerializeField] private GameObject prefab;
        [Header("Bullet Properties")]
        [SerializeField] private float damage = 1f;
        [SerializeField] private float speed = 1f;

        #region Attributes, Getters & Setters
        public float Speed => speed;
        public GameObject Prefab => prefab;
        #endregion
        #endregion
    }
}
