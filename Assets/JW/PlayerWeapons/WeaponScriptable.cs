using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Project3.Weapons
{
    /// <summary>
    /// Author: JW
    /// The ScriptableObject template for the Player's weapons
    /// </summary>
    [CreateAssetMenu(fileName = "PlayerWeapon", menuName = "ScriptableObjects/Player/Weapon", order = 1)]
    public class WeaponScriptable : ScriptableObject
    {
        #region Variables
        [Header("Time")]
        [SerializeField] private float fireRate = 1f;
        [SerializeField] private float magazineReloadTime = 2f;
        [SerializeField] private float currentTime = 0f;
        [Header("Ammunition")]
        [SerializeField] private int currentAmmoCount = 100;
        [SerializeField] private int magazineCapacity = 10;
        [SerializeField] private int currentMagazineCount = 10;
        [Header("Defaults")]
        [SerializeField] private int defualtAmmoCount = 100;
        #endregion

        #region Public Functions
        /// <summary>
        /// Resets all changed values to a defualt
        /// </summary>
        public void LoadDefualts()
        {
            // Ammo defualts
            currentAmmoCount = defualtAmmoCount;
            currentMagazineCount = magazineCapacity;

            // Time defaults
            currentTime = fireRate;
        }

        /// <summary>
        /// Update the fire rate cooldown and manages ammo calculationns
        /// </summary>
        /// <param name="dt">float | how much time since last update</param>
        /// <returns>bool | true if we finished the cooldown</returns>
        public bool UpdateCooldown(float dt)
        {
            currentTime -= dt; // Update cooldown
            if (currentTime <= 0f) // Check if we made cooldown
            {
                // reset cooldown
                currentTime = fireRate;

                // Ammunition calculations
                currentMagazineCount--;
                if (currentMagazineCount <= 0) // Check for empty magazine
                {
                    Debug.Log("Reloading");
                    currentTime = magazineReloadTime;

                    // Reload if we have enough
                    if (currentAmmoCount < magazineCapacity)
                    {
                        currentMagazineCount = currentAmmoCount; // Put all ammo into the magazine
                        currentAmmoCount = 0; // Deplete all ammo
                    }
                    else
                    {
                        currentAmmoCount -= magazineCapacity; // Take a full magaizine's worth of ammo from the total
                        currentMagazineCount = magazineCapacity; // Refill the magazine with it
                    }
                }

                Debug.Log($"{name} fired");
                return true; // Flag that we fired
            }
            else
            {
                return false; // Flag that we did not fire
            }
        }
        #endregion
    }
}
