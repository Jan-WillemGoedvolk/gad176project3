using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Project3.Movement.Wrapping
{
    /// <summary>
    /// Author: JW
    /// Attach this script to an object to allow for screen wrapping
    /// </summary>
    /// <remarks>
    /// Can independentally enable or disable horizontal and vertical wrapping
    /// For the moment, manually set the `wrapBox` values to match the camera view
    /// `cam` is the camera we are looking through
    /// </remarks>
    public class ScreenWrapper : MonoBehaviour
    {
        #region Variables
        [Header("Screen Wrapper")]
        [SerializeField] private bool wrapHorizontal = true; // Wraps in the x direction
        [SerializeField] private bool wrapVertical = true; // Wraps in the y direction 
        [SerializeField] private Vector2 wrapBox = Vector2.one; // Bounding box to check for wrapping
        [SerializeField] private Camera cam; // Camera to center the bounding box around
        #endregion

        #region Private Functions
        /// <summary>
        /// Wraps the given GameObject in the allowed wrapping directions
        /// </summary>
        /// <param name="objectToWrap">GameObject | The GameObject to wrap</param>
        private void Wrap(GameObject objectToWrap)
        {
            if (wrapVertical)
            {
                if (objectToWrap.transform.position.y >= wrapBox.y) // Wrap from top to bottom
                {
                    objectToWrap.transform.position = new Vector2(objectToWrap.transform.position.x, 0);
                    Debug.Log("Wrapped y >");
                }
                else if (objectToWrap.transform.position.y <= 0) // Wrap from bottom to top
                {
                    objectToWrap.transform.position = new Vector2(objectToWrap.transform.position.x, wrapBox.y);
                    Debug.Log("Wrapped y <");
                }
            }

            if (wrapHorizontal)
            {
                if (objectToWrap.transform.position.x >= wrapBox.x / 2) // Wrap from right to left
                {
                    objectToWrap.transform.position = new Vector2(-wrapBox.x/2 + 0.1f, objectToWrap.transform.position.y);
                    Debug.Log("Wrapped x >");
                }
                else if (objectToWrap.transform.position.x <= -wrapBox.x / 2) // Wrap from left to right
                {
                    objectToWrap.transform.position = new Vector2(wrapBox.x/2 - 0.1f, objectToWrap.transform.position.y);
                    Debug.Log("Wrapped x <");
                }
            }
        }
        #endregion

        #region Unity Specific Functions
        private void FixedUpdate()
        {
            Wrap(gameObject);
        }
        #endregion

        #region Debugging
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireCube(cam.transform.position, wrapBox); // Draw box for where it will wrap from
        }
        #endregion
    } 
}
