using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Hamad.Project3.Enemy;

namespace JW.Project3.Movements.Turning
{
    /// <summary>
    /// Author: JW
    /// Turns an object
    /// </summary>
    public class Turner : MonoBehaviour
    {
        #region Variables
        [Header("Object Turner Settings")]
        [Header("Speed")]
        [SerializeField] private float rotateSpeed = 0f;
        [SerializeField] private float maxRotateSpeed = 0f;
        [SerializeField] private bool isClockwise = true;
        [Header("Object")]
        [SerializeField] private Rigidbody2D rb;
        #endregion

        #region Public Functions
        /// <summary>
        /// Turns the object using the settings of the script
        /// </summary>
        public void Turn()
        {
            if (!isClockwise)
            {
                rb.AddTorque(-rotateSpeed);
            }
            else
            {
                rb.AddTorque(rotateSpeed);
            }
            rb.angularVelocity = Mathf.Clamp(rb.angularVelocity, 0f, maxRotateSpeed);
        }
        #endregion

        #region Unity Specific Functions
        private void Awake()
        {
            if (rb == null)
            {
                Debug.LogError($"ERROR [{gameObject.name}][Turner] No Rigidbody found");
            }
        }

        private void Update()
        {
            Turn();
        }
        #endregion
    }
}
