using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.VisualScripting;
using UnityEngine;

namespace JW.Project3.Movement.Player
{
    /// <summary>
    /// Author: JW
    /// Handles the movement for the Player GameObject
    /// </summary>
    /// <remarks>
    /// Made by JW.
    /// Requirements:
    /// - This script uses kinematic movement
    /// - The object this script is attached to needs a Rigidbody2D component as well
    /// - Needs at least 1 movementSpeeds vector to start
    /// - Needs at least 1 gravityRanges float to start
    /// - verticalKeys and horizontalKeys needs to have an even number of elements. They work in pairs of 2 so that the first value is the negative key and the second the positive key
    /// - gravityRanges is a list of floats for distance thresholds from the planet we need to be before switching movement speed vectors.
    /// - Values in gravityRanges are assumed to be in order from smallest to largest, if it isn't in this order then satisfaction garuntee is voided
    /// </remarks>
    public class PlayerMovement : MonoBehaviour
    {
        #region Variables
        [Header("Player Components Needed")]
        [SerializeField] protected Rigidbody2D objectToMove; // The object should have a Rigidbody2D for collision so we will use physics based movement
        //[SerializeField] protected Gravity gravity;
        [Header("Player Speeds")]
        [SerializeField] protected Transform planet;
        [SerializeField] private Rigidbody2D planetRb;
        [SerializeField] private int speedIndex = 0;
        [SerializeField, ReadOnly(true)] private Vector2 movementDirection = Vector2.zero;  // The direction the Player will move in
        [Header("New Movement")]
        /**
        [SerializeField] private float minDistance = 4f;
        [SerializeField] private float maxDistance = 10f;
        [SerializeField] private float minSpeed = 4f;
        [SerializeField] private float gradient;
        [SerializeField] private float c;
        */
        [SerializeField] private float maxSpeed = 4f;
        [SerializeField] private float massProduct;
        [SerializeField] private float g = 9.8f;
        
        [Header("Player Movement Keys")]
        [SerializeField] private List<KeyCode> verticalKeys = new List<KeyCode>(); // Keys to move around vertically. {+ve, -ve, ...}
        [SerializeField] private List<KeyCode> horizontalKeys = new List<KeyCode>(); // Keys to move around horizontally. {+ve, -ve, ...}
        [Header("Debugging")]
        [SerializeField] private KeyCode debugKey;
        #endregion

        #region Public Functions
        /// <summary>
        /// Moves the Player object in the given direction. This direction also doubles as the speed to move it at
        /// </summary>
        /// <param name="direction">Vetor2 | Direction and speed to move the Player object in</param>
        public void Move(Vector2 direction)
        {
            //objectToMove.velocity = direction * CalculateSpeed(Vector2.Distance(transform.position, planet.position));
            //objectToMove.velocity = direction * Mathf.Lerp(maxSpeed, minSpeed, Vector2.Distance(transform.position, planet.position) + minDistance); 
            objectToMove.velocity = direction;
            objectToMove.velocity = Vector2.ClampMagnitude(objectToMove.velocity, maxSpeed);
        }
        #endregion

        #region Unity Specific Functions
        protected virtual void Awake()
        {
            // Validation Checks
            #region Validation Checks
            if (objectToMove == null) objectToMove = GetComponent<Rigidbody2D>(); // Get the current game object's Rigidbody2D

            // Planet
            GameObject planetObject = GameObject.FindGameObjectWithTag("Planet");
            if (planetObject == null) Debug.LogError($"ERROR |{name}| No GameObject with tage 'Planet' found in scene");
            if (planet == null) planet = planetObject.GetComponent<Transform>(); 
            if (planetRb == null) planetRb = planetObject.GetComponent<Rigidbody2D>();
            #endregion


            /*
            // Calculate movement constants
            // y = mx + c
            float dy = maxSpeed - minSpeed;
            float dx = maxDistance - minDistance;
            gradient = dy / dx; // m
            Debug.Log($"gradient = dy / dx | {gradient} = {dy} / {dx}");
            // minSpeed = (gradient * minDistance) + c
            // c = minSpeed - (gradient * minDistance)
            c = minSpeed - (gradient * minDistance);
            */
        }

        // Start is called before the first frame update
        void Start()
        {
            #region Validation Checks
            Debug.Assert(objectToMove != null, "ERROR |PlayerMovement| No Rigidbody2D found on the PlayerMovement script");
            Debug.Assert(planet != null, "ERROR |PlayerMovement| No planet transform found on the PlayerMovement script");
            #endregion
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            // Getting the movement inputs
            movementDirection.x = GetAxisInput(horizontalKeys, false);
            movementDirection.y = GetAxisInput(verticalKeys, true);

            float distance = Vector2.Distance(transform.position, planet.position);
            //movementDirection = movementDirection * CalculateGravity(planet, transform).magnitude;
            movementDirection = movementDirection * CalculateSpeed(distance, g);
        }

        protected virtual void FixedUpdate()
        {
            Move(movementDirection); // Aplying the movement
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Gets the movement speed in a given direction from the list of keys provided
        /// </summary>
        /// <param name="keys">List<KeyCodes> | A list of keys to check</KeyCodes></param>
        /// <param name="isVertical">bool | Whether to check for vertical keys or horizontal keys</param>
        /// <returns></returns>
        private float GetAxisInput(List<KeyCode> keys, bool isVertical)
        {
            for (int i = 0; i < keys.Count; i += 2) // Check each pair of keys, since it should be +ve key then -ve key
            {
                if (Input.GetKey(keys[i])) // Check for +ve
                {
                    return 1f;
                }
                else if (Input.GetKey(keys[i + 1])) // Check for -ve
                {
                    return -1f;
                }
            }
            return 0f; // If we didn't press anything, return 0
        }

        private float CalculateSpeed(float distance, float g)
        {
            massProduct = planetRb.mass * objectToMove.mass;

            // F = G * ((m1 * m2) / r^2)
            // force = g * (massProduct / distance^2)
            float force = g * (massProduct / (distance * distance));
            Debug.Log($"{force} = {g} * ({massProduct} / {distance}^2)");
            return force;
        }
        #endregion

        #region Debugging
        private void OnDrawGizmosSelected()
        {
            //foreach (var band in gravityRanges)
            //{
            //    Gizmos.DrawWireSphere(planet.position, band);
            //}

            //Gizmos.DrawWireSphere(planet.position, minDistance);
            //Gizmos.DrawWireSphere(planet.position, maxDistance);
        }
        #endregion
    }
}
