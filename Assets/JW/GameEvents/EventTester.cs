using JW.Project3.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JW.Project3.Debugging
{
    public class EventTester : MonoBehaviour
    {
        #region Variables
        public GameEventListener listener;
        public GameEvent listening;
        public List<KeyCode> debugKeys = new List<KeyCode>();
        #endregion

        #region Unity Specific Functions
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(debugKeys[0]))
            {
                listening.Raise();
            }
        } 
        #endregion
    } 
}
