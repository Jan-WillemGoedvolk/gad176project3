# Feature Name
## Script Name
Outline of what the script does
### Function Name
Outline of what the function does
#### Inputs
- list of inputs to the function or `void` if no inputs
#### Outputs
- Outputs or `void` if no output

---
---

# Movement
## Turner
Turns an object at a set speed to a max speed
### Turn
Turns an object at a set speed to a max speed
#### Inputs
`void`
#### Outputs
`void`

---

## PlayerMovement
Handles moving the Player GameObject with keyboard inputs
> Notes:
> Uses kinematic movement
> Attached object needs a Rigidbody2D component
> Needs at least 1 `gravityRanges` element to start
> Elements in `gravityRanges` are assumed to be in ascending order (smallest to largest)
> Needs at least 1 `movementSpeeds` element to start
> Needs a `movementSpeeds` element for every `gravityRanges` element
> `vertialKeys` and `horizontalKeys` need to have an even number of elements
>> Elements work in pairs of 2. the first key is the negative key and the second key is the positive key
### Move
Moves the player object in the given direction
#### Inputs
- `Vector2` direction: The directio  to move in. The vector's magnitude acts as the speed
#### Outputs
`void`

---

## ScreenWrapper
Allows for GameObjects to wrap around the screen. Can choose to wrap horizontally and/or vertically
> This is easiest when used with heads-on orthographic view setting in the camera
> You will need to manualy set the size of `wrapBox`
>> list of common values

---

## Gravity
Simulates a gravitational pull between 2 `GameObjects`
### CalculateGravity
Calculates the gravitational pull vector between the provided `Transform`s
#### Inputs
- `Transform` planet: Transform of the object to orbit
- `Transform` orbitter: Transform of the thing that is orbiting the planet
#### Outputs
`Vector2` The vector of gravitational pull towards the planet `Transform`

---
---

# Events
## EventTester
A script purely for helping with debugging event system if needed.

---

## GameEvent
Creates an event that can be raised to call all responses from the current listeners.
### Raise
Calls all listeners for this event to trigger their Response events
#### Inputs
`void`
#### Outputs
`void`
### AddListener
Adds a `GameEventListener` to the list of listeners
#### Inputs
`GameEventListener` newListener: The new `GameEventListener` to add
#### Outputs
`void`
### RemoveListener
Removes the given `GameEventListener` from the list of listeners
#### Inputs
`GameEventListener` listener: The new `GameEventListener` to remove
#### Outputs
`void`
### IsListening
Checks whether the given GameEventListener is in the list
#### Inputs
`GameEventListener` listener: the listener to check for
#### Outputs
`bool` true if its in the list, otherwise false
### CountListeners
Returns how many `GameEventListener` are in the list
#### Inputs
`void`
#### Outputs
`int` number of listeners

---

## GameEventListener
This script listens for a given GameEvent to be Raised to perform all the given responses
### OnRaised
Calls all the `UnityEvent` events in response to being raised.
#### Inputs
`void`
#### Outputs
`void`

---
---

# Player Weapons
## Weapons
Holds the logic and functionality for the weapons and ammo and the currently equiped ones
### Shoot
Spawns and sets up a bullet
#### Inputs
`void`
#### Outputs
`void`

---

## WeaponScriptable
The ScriptableObject template for the Player's weapons
### LoadDefualts
Resets all changed values to a defualt
#### Inputs
`void`
#### Outputs
`void`
### UpdateCooldown
Update the fire rate cooldown and manages ammo calculationns
#### Inputs
`float` dt: How much time has passed since cooldown was last updated
#### Outputs
`bool` true when the weapon fired, false otherwise

---

## AmmoScriptable
ScriptableObjecct template for Player bullets

---

## AmmoMono
The Monobehaviour script for all bullet GameObjects to be able to detect collision and move downard. Inherits from `Gravity`
### Setup
Sets up the neccesary components for this ammo object
#### Inputs
`AmmoScriptable` equipedAmmo: The currently equiped `AmmoScriptable` item from `Weapons`
#### Outputs
`void`

---
---

# HealthManager
## HealthManager
Manage the health of the player. It provides a way to control the object's health, including healing and handling death.
### Health
Getter and setter that allows other scripts to change the `HealthManager`'s health variable.
Clamps `health` to `maxHealth` and checks for death, destroying the object if dead, whenever `health` is set.
#### Inputs
`float` How much health to add or take away.
#### Outputs
`float` current health of the `HealthManager`
### ChangeHealth
Changes the `health` using the `Health` getter and setter
#### Inputs
`float` amount: Amount of health to add or take away
#### Outputs
`void`

---

## PartialHeal
provide functionality for healing the player by a percentage of its current health when it collides with a healing object. This script inherits from the `HealthManager`.

---

## Healing
The `Healing` script aims to provide functionality for healing the player by a specified amount no matter the currently available health when a collision occurs with a healing object. Inherits from `HealthManager`.

---

## SpawnHealing
Spawning healing power-ups within a defined area at custom intervals. It utilizes the `PartialHeal` and `Healing` prefabs in Unity Scene.
### SpawnPowerUp
This function is responsible for spawning the healing power-ups.
#### Inputs
`void`
#### Outputs
`void`

---